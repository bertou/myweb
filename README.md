# MyWeb

Simple PHP7 webpage headers and footers based on Bootstrap 4.
Uses:
*  bootstrap 4 as main template (including jQuery et al.)
*  aos 2 for animation on scrolling
*  fontawesome 5